package main

type WorkflowAbstract struct {
	Uid                 string           `json:"uid"`         // Uid univoco workflow astratto
	Comment             string           `json:"comment"`     //
	Numelem             int              `json:"numelem"`     // serve ?
	StartAt             string           `json:"startat"`     // può essere anche più di una la sorgente in realtà
	ServicesAbstract    VertexesAbstract `json:"services"`    // elenco dei Vertexes, e quindi un elenco completo
	ConnectionsAbstract EdgesAbstract    `json:"connections"` // gli archi, per costruire il path
	Created             string           `json:"created"`
	Author              string           `json:"author"`
	IsDeploy            bool             `json:"isdeploy"`
}

func makeWorkflowAbstract(uid string, comment string, numelem int, startat string,
	servicesAbstract VertexesAbstract, connectionsAbstract EdgesAbstract, created string,
	author string) WorkflowAbstract {
	return WorkflowAbstract{
		Uid:                 uid,
		Comment:             comment,
		Numelem:             numelem,
		StartAt:             startat,
		ServicesAbstract:    servicesAbstract,
		ConnectionsAbstract: connectionsAbstract,
		Created:             created,
		Author:              author,
	}
}

type VertexAbstract struct {
	// id vertex (magari un numero da 0 ++)
	Name         string   `json:"name"`         // nome del servizio concreto (e lui sceglie la replica)
	Method       string   `json:"method"`       // GET/POST/PUT/DELETE
	Url          string   `json:"url"`          //Uid    string   `json:"uid"`
	Order        string   `json:"order"`        // Ordine con cui deve essere eseguita la chiamata es. 1, 2, 3...
	To           []string `json:"to"`           // A chi il vertex passa il suo output.. in questo caso potrei pensare a indicate il "name" astratto (Forse si può dedurre questo campo dagli archi in realtà...!!!)
	Microservice string   `json:"microservice"` // E' il nome del microservizio invocato su gokit
	IsBody       bool     `json:"isbody"`       //
	Body         string   `json:"body"`         // json body in rappresentazione stringa..
	IsParameters bool     `json:"isparameters"` //
	Parameters   []string `json:"parameters"`   // Eventuali parametri da passare all' url
	IsUrlConcat  bool     `json:"isurlconcat"`  //
	UrlConcat    []string `json:"urlconcat"`    // Eventuali parametri da passare su linea
	End          bool     `json:"end"`          // campo che forse è inutile
	Type         string   `json:"type"`         // (if/else condizionale) oppure { "microservice", "???"  }
	IsBound      bool     `json:"isbound"`      // sarà da settare a True se va a essere mappato su un service concreto
	BoundTo      string   `json:"boundto"`      // corrisponde al service che salta fuori durante la risoluzione del grafo
}

func makeVertexAbstract(name string, method string, url string, order string, to []string, microservice string, isBody bool,
	body string, isParameters bool, parameters []string, isUrlConcat bool, urlConcat []string, end bool, t string, isBound bool,
	boundTo string) VertexAbstract {
	return VertexAbstract{
		Name:         name,
		Method:       method,
		Url:          url,
		To:           to,
		Microservice: microservice,
		IsBody:       isBody,
		Body:         body,
		IsParameters: isParameters,
		Parameters:   parameters,
		IsUrlConcat:  isUrlConcat,
		UrlConcat:    urlConcat,
		End:          end,
		Type:         t,
		IsBound:      isBound,
		BoundTo:      boundTo,
	}
}

// coppie di vertex
type EdgeAbstract struct {
	From VertexAbstract `json:"from"`
	To   VertexAbstract `json:"to"`
}

func makeEdgeAbstract(from VertexAbstract, to VertexAbstract) EdgeAbstract {
	return EdgeAbstract{
		From: from,
		To:   from,
	}
}

type WorkflowsAbstract []WorkflowAbstract
type VertexesAbstract []VertexAbstract
type EdgesAbstract []EdgeAbstract
