package main

type DeployTask struct {
	Uid         string `json:"uid"`
	Comment     string `json:"comment"`
	UidWorkflow string `json:"uidWorklow"`
	IdWorklowDb string `json:"idWorklowDb"`
	Action      string `json:"action"`
	Created     string `json:"created"`
	Author      string `json:"author"`
	Status      string `json:"status"`
}

func makeDeployTask(uid string, comment string, uidWorkflow string, idWorkflowDb string,
	action string, created string, author string, status string) DeployTask {
	return DeployTask{
		Uid:         uid,
		Comment:     comment,
		UidWorkflow: uidWorkflow,
		IdWorklowDb: idWorkflowDb,
		Action:      action,
		Created:     created,
		Author:      author,
		Status:      status,
	}
}

type DeployTasks []DeployTask

type LogTask struct {
	Uid     string `json:"uid"`
	Type    string `json:"type"`
	Message string `json:"message"`
	Node    string `json:"node"`
	Service string `json:"service"`
	Ip      string `json:"ip"`
	File    string `json:"file"`
	Line    string `json:"line"`
	Created string `json:"created"`
	Author  string `json:"author"`
	Status  string `json:"status"`
}

type LogTasks []LogTask

/**
 * Costruttore
 */

func makeLogTask(uid string, t string, message string, node string, service string, ip string, file string, line string, created string,
	author string, status string) LogTask {
	return LogTask{
		Uid:     uid,
		Type:    t,
		Message: message,
		Node:    node,
		Service: service,
		Ip:      ip,
		File:    file,
		Line:    line,
		Created: created,
		Author:  author,
		Status:  status,
	}
}

type RepairTask struct {
	Uid         string `json:"uid"`
	Comment     string `json:"comment"`
	UidWorkflow string `json:"uidWorklow"` //uid del workflow
	IdWorklowDb string `json:"idWorklowDb"`
	Action      string `json:"action"`
	Created     string `json:"created"`
	Author      string `json:"author"`
	Status      string `json:"status"`
}

func makeRepairTask(uid string, comment string, uidWorkflow string, idWorkflowDb string,
	action string, created string, author string, status string) RepairTask {
	return RepairTask{
		Uid:         uid,
		Comment:     comment,
		UidWorkflow: uidWorkflow,
		IdWorklowDb: idWorkflowDb,
		Action:      action,
		Created:     created,
		Author:      author,
		Status:      status,
	}
}

type RepairTasks []RepairTask
